<?php

return [
    'oracle' => [
        'driver'         => 'oracle',
        'host'           => env('ORA_HOST', ''),
        'port'           => env('ORA_PORT', ''),
        'database'       => env('ORA_DATABASE', ''),
        'username'       => env('ORA_USERNAME', ''),
        'password'       => env('ORA_PASSWORD', ''),
        'service_name'   => env('ORA_SERVICE', ''),
        'charset'        => env('ORA_CHARSET', ''),
        'prefix'         => env('ORA_PREFIX', ''),
        'prefix_schema'  => env('ORA_SCHEMA_PREFIX', ''),
        'edition'        => env('ORA_EDITION', ''),
        'server_version' => env('ORA_SERVER_VERSION', ''),
    ],
];
