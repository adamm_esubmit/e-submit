var mainApp = angular.module('mainApp', ['ui.bootstrap', 'ui.tree'], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('^(');
    $interpolateProvider.endSymbol(')^');
});

mainApp.run(function ($rootScope, $location) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $rootScope.notification = function (msg) {
        $.growl({
            icon: 'feather icon-layers',
            title: '&nbsp;Notifications',
            message: '<br>' + msg,
            url: ''
        }, {
                element: 'body',
                type: 'inverse',
                allow_dismiss: true,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                offset: {
                    x: 30,
                    y: 30
                },
                spacing: 10,
                z_index: 999999,
                delay: 2500,
                timer: 100000000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                    enter: 'animated flipInX',
                    exit: 'animated flipOutX'
                },
                icon_type: 'class',
                template: '<div data-growl="container" style="width:600px!important;" class="alert" role="alert">' +
                    '<button type="button" class="close" data-growl="dismiss">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '<span class="sr-only">Close</span>' +
                    '</button>' +
                    '<span data-growl="icon"></span>' +
                    '<span data-growl="title"></span>' +
                    '<span data-growl="message"></span>' +
                    '<a href="#!" data-growl="url"></a>' +
                    '</div>'
            });
    };

    $rootScope.block = function () {
        $.blockUI({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
    };
    $rootScope.unblock = function () {
        $.unblockUI();
    };

    $rootScope.blockcomponent = function (attr) {
        $("#" + attr).block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
    };
    $rootScope.unblockcomponent = function (attr) {
        $("#" + attr).unblock();
    };
});

mainApp.controller('homeCtrl', function ($scope, $rootScope) {
    $("#select-unitworkos").select2({
        allowClear: true,
        placeholder: "select"
    });

    $("#select-unitworkos").change(function () {
        var unitwork = $(this).val();
        var data = JSON.parse($("#jsondataunit").val());
        if (unitwork != "") {
            unitwork = unitwork.split("_")[0];
            $.each(data, function (index, item) {
                if (parseInt(item.c_unitwork_id) == parseInt(unitwork)) {
                    $("#comp_name").val(item.companyname);
                    $("#hold_name").val(item.holdingname);
                }
            });
        }
    });

    $scope.openshift = function () {
        $rootScope.block();
        unitwork = $("#select-unitworkos").val();
        unit = "";
        company = "";
        holding = "";

        if (unitwork != "" || unitwork != null) {
            unit = unitwork.split("_")[0];
            company = unitwork.split("_")[1];
            holding = unitwork.split("_")[2];
        }
        $.ajax({
            type: "POST",
            url: '/open-shift',
            data: {
                unitwork: unit,
                company: company,
                holding: holding
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                if (parseInt(response.sts) == 1) {
                    location.reload();
                }
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            },
        });
    };

    $scope.useshift = function (holding, company, unitwork) {
        $rootScope.block();
        $.ajax({
            type: "POST",
            url: '/use-shift',
            data: {
                unitwork: unitwork,
                company: company,
                holding: holding
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                if (parseInt(response.sts) == 1) {
                    location.reload();
                }
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            },
        });
    };
});
