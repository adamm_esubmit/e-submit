mainApp.controller('userCtrl', function ($scope, $rootScope) {
    var IdEdit = -1;
    var isActivate;
    $scope.editform = false;

    $("#select-sex").select2({
        allowClear: true,
        placeholder: "select"
    });
    $('#dob').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
        format: 'YYYY-MM-DD'
    }).on('change', function (e, date) {
        var a = new Date(date);
        year = a.getFullYear();
        month = (a.getMonth() + 1) < 10 ? "0" + (a.getMonth() + 1) : (a.getMonth() + 1);
        date = a.getDate() < 10 ? "0" + a.getDate() : a.getDate();
        $("#dobsend").val(year + "-" + month + "-" + date);
    });;
    
    /* User Table Setting */

    var table = $("#user-table").DataTable({
        autoWidth: true,
        stateSave: true,
        pagingType: "simple",
        language: { paginate: { 'next': 'Next &rarr;', 'previous': '&larr; Prev' } },
        processing: true,
        serverSide: true,
        ajax: "user-table",
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],

        "columns": [
            { "data": "id", "className": "not-show" },
            { "data": "username" },
            { "data": "name" },
            { "data": "email" },
            { "data": "gender" },
            { "data": "dob" },
            { "data": "last_login" },
            { "data": "status", "className": "dt-center" },
            { "data": "action", "className": "dt-center" }
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $scope.modalAdd = function () {
        IdEdit = -1;
        $scope.editform = true;
        clearForm();
        getUsersUnitwork(-1);
    };
    $('body').on('click', '#user-table_wrapper .btn-edit', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $scope.editform = true;
        $("#username").focus();
        var name = data.name.split(" ");
        $("#username").val(data.username);
        $("#firstname").val(name[0]);
        $("#lastname").val(name[1]);
        $("#email").val(data.email);
        $("#select-sex").val(data.gender).trigger("change");
        tanggal = data.dob.split("/");
        $("#dob").val(tanggal[2] + "-" + tanggal[1] + "-" + tanggal[0]);
        // GET ALL USERS UNITWORK
        getUsersUnitwork(IdEdit);

    });
    $('body').on('click', '#user-table_wrapper .btn-disable', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        isActivate = 0;
        $("#title_txt").text('Are you sure to disable user ' + data.username + ' ?');
        $("#modal-activate").modal('show');
    });
    $('body').on('click', '#user-table_wrapper .btn-enable', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        isActivate = 1;
        $("#title_txt").text('Are you sure to enable user ' + data.username + ' ?');
        $("#modal-activate").modal('show');
    });
    $('body').on('click', '#user-table_wrapper .btn-repass', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $("#title_tt").text('Are you sure to reset password ' + data.username + ' ?');
        $("#modal-repass").modal('show');
    });
    $('body').on('click', '#user-table_wrapper .btn-unlock', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $("#title_ttt").text('Are you sure to unlock user ' + data.username + ' ?');
        $("#modal-unlock").modal('show');
    });

    // Save User
    $scope.save = function () {
        var _url = IdEdit == -1 ? 'user-insert' : 'user-update';
        $rootScope.block();
        $.ajax({
            type: "POST",
            url: _url,
            data: {
                id: IdEdit,
                username: $("#username").val(),
                firstname: $("#firstname").val(),
                lastname: $("#lastname").val(),
                email: $("#email").val(),
                sex: $("#select-sex").val(),
                dob: $("#dobsend").val(),
                unitusers: $("#select-usersunit").val()
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                if (parseInt(response.sts) == 1) {
                    clearForm();
                    table.ajax.reload();
                    $scope.editform = false;
                }
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            },
        });
    };
    // Reactivate User
    $scope.activateuser = function () {
        $rootScope.blockcomponent('modal-activate');
        $.ajax({
            type: "POST",
            url: 'user-activate',
            data: {
                id: IdEdit,
                sts: isActivate
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $rootScope.unblockcomponent('modal-activate');
                table.ajax.reload();
                $("#modal-activate").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblockcomponent('modal-activate');
                $("#modal-activate").modal('hide');
            },
        });
    };
    // Reset Password
    $scope.resetpassword = function () {
        $rootScope.blockcomponent('modal-repass');
        $.ajax({
            type: "POST",
            url: 'user-resetpassword',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $rootScope.unblockcomponent('modal-repass');
                $("#modal-repass").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblockcomponent('modal-repass');
                $("#modal-repass").modal('hide');
            },
        });
    };
    // Unlock User
    $scope.unlockuser = function () {
        $rootScope.blockcomponent('modal-unlock');
        $.ajax({
            type: "POST",
            url: 'user-unlock',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $rootScope.unblockcomponent('modal-unlock');
                table.ajax.reload();
                $("#modal-unlock").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblockcomponent('modal-unlock');
                $("#modal-unlock").modal('hide');
            },
        });
    };
    function getUsersUnitwork(userId) {
        $.ajax({
            type: "GET",
            url: 'usersunitwork-get?userid=' + userId,
            success: function (rsp) {
                data = rsp.data;
                $('#select-usersunit').find('option').remove();
                for (i = 0; i < data.length; i++) {
                    if (data[i].selected == true) $('#select-usersunit').append('<option value="' + data[i].c_id + '" selected="selected">' + "(" + data[i].c_code + ") " + data[i].c_name + '</option>');
                    else $('#select-usersunit').append('<option value="' + data[i].c_id + '">' + "(" + data[i].c_code + ") " + data[i].c_name + '</option>');
                }
                $('#select-usersunit').multiSelect({
                    selectableHeader: "<div class='custom-header'>All Unitwork</div>",
                    selectionHeader: "<div class='custom-header'>Selected Unitwork</div>",
                });
                $('#select-usersunit').multiSelect('refresh');
                $scope.$apply();
            },
            error: function (err) {
            },
        });
    };
    function clearForm() {
        $("#username").val("");
        $("#email").val("");
        $("#select-holding").val("").trigger("change");
        $("#select-company").val("").trigger("change");
        $("#select-unitwork").val("").trigger("change");
    };

});