mainApp.controller('rolePermissionCtrl', function ($scope, $rootScope) {
    var IdEdit;
    $scope.isEdit = 0;
    $scope.deserializeData = [];

    /* User Table Setting */
    var table = $("#role-table").DataTable({
        autoWidth: true,
        stateSave: true,
        pagingType: "simple",
        language: { paginate: { 'next': 'Next &rarr;', 'previous': '&larr; Prev' } },
        processing: true,
        serverSide: true,
        ajax: "rolepermission-table",
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "created_at" },
            { "data": "updated_at" },
            { "data": "action", "className": "dt-center" }
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $('body').on('click', '#role-table_wrapper .btn-edit', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#role-table").dataTable().fnGetPosition(tr);
        var data = $("#role-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $.ajax({
            type: "GET",
            url: 'rolepermission-get?roleid=' + data.id,
            success: function (rsp) {
                var response = JSON.parse(rsp);
                $scope.data = response;
                $scope.isEdit = 1;
                $scope.deserializeData = [];
                $scope.$apply();
            },
            error: function (err) {
            },
        });

    });

    $scope.save = function () {
        $.blockUI({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        $scope.deserializeTree($scope.data);
        $scope.sendData = [];
        $.each($scope.deserializeData, function (index, item) {
            if (item.selected)
                $scope.sendData.push(item.id);
        });
        $.ajax({
            type: "POST",
            url: 'rolepermission-save',
            data: {
                roleId: IdEdit,
                rolepermissions: $scope.sendData
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $scope.isEdit = 0;
                $.unblockUI();

            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $.unblockUI();
            },
        });
    };

    $scope.deserializeTree = function (treeData) {
        $.each(treeData, function (index, item) {
            $scope.deserializeData.push(item);
            if (item.childs.length > 0) {
                $scope.deserializeTree(item.childs);
            }
        });
    }
});