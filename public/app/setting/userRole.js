mainApp.controller('userRoleCtrl', function ($scope, $rootScope) {
    var IdEdit;
    var isActivate;
    $('#role-users-ed').multiSelect({
        selectableHeader: "<div class='custom-header'>All Roles</div>",
        selectionHeader: "<div class='custom-header'>Selected Roles</div>",
    });
    /* User Table Setting */
    var table = $("#user-table").DataTable({
        autoWidth: true,
        stateSave: true,
        pagingType: "simple",
        language: { paginate: { 'next': 'Next &rarr;', 'previous': '&larr; Prev' } },
        processing: true,
        serverSide: true,
        ajax: "userrole-table",
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],
        "columns": [
            { "data": "id", "className": "not-show" },
            { "data": "username" },
            { "data": "email" },
            { "data": "last_login" },
            { "data": "status", "className": "dt-center" },
            { "data": "action", "className": "dt-center" }
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $('body').on('click', '#user-table_wrapper .btn-edit', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#user-table").dataTable().fnGetPosition(tr);
        var data = $("#user-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $.ajax({
            async: true,
            type: "GET",
            url: 'userroles-get?userid=' + IdEdit,
            success: function (rsp) {
                $('#role-users-ed').find('option').remove();
                for (i = 0; i < rsp.roles.length; i++) {
                    if (rsp.roles[i].selected) {
                        $('#role-users-ed').append('<option value="' + rsp.roles[i].c_id + '" selected="selected">' + rsp.roles[i].c_name + '</option>');
                    }
                    else {
                        $('#role-users-ed').append('<option value="' + rsp.roles[i].c_id + '">' + rsp.roles[i].c_name + '</option>');
                    }
                }
                $('#role-users-ed').multiSelect('refresh');
                $scope.$apply();
                $("#modal-edit").modal('show');
            },
            error: function (err) {
                $("#main-form").unblock();
            },
        });

    });

    $scope.save = function () {
        $('#modal-edit').block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        $.ajax({
            type: "POST",
            url: 'userrole-save',
            data: {
                id: IdEdit,
                userroles: $("#role-users-ed").val()
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#modal-edit").unblock();
                $("#modal-edit").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $("#modal-edit").unblock();
                $("#modal-edit").modal('hide');
            },
        });
    };
    // Reactivate User
    $scope.activateuser = function () {
        $('#modal-activate').block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        $.ajax({
            type: "POST",
            url: 'user-activate',
            data: {
                id: IdEdit,
                sts: isActivate
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#modal-activate").unblock();
                table.ajax.reload();
                $("#modal-activate").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $("#modal-activate").unblock();
                $("#modal-activate").modal('hide');
            },
        });
    };
    // Reset Password
    $scope.resetpassword = function () {
        $('#modal-repass').block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        $.ajax({
            type: "POST",
            url: 'user-resetpassword',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#modal-repass").unblock();
                $("#modal-repass").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $("#modal-repass").unblock();
                $("#modal-repass").modal('hide');
            },
        });
    };
    // Unlock User
    $scope.unlockuser = function () {
        $('#modal-unlock').block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });
        $.ajax({
            type: "POST",
            url: 'user-unlock',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#modal-unlock").unblock();
                table.ajax.reload();
                $("#modal-unlock").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $("#modal-unlock").unblock();
                $("#modal-unlock").modal('hide');
            },
        });
    };
});