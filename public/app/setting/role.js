mainApp.controller('roleCtrl', function ($scope, $rootScope) {
    var IdEdit = -1;
    $scope.editform = false;

    /* User Table Setting */
    var table = $("#role-table").DataTable({
        autoWidth: true,
        stateSave: true,
        pagingType: "simple",
        language: { paginate: { 'next': 'Next &rarr;', 'previous': '&larr; Prev' } },
        processing: true,
        serverSide: true,
        ajax: "role-table",
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "created_at" },
            { "data": "updated_at" },
            { "data": "action", "className": "dt-center" }
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $('body').on('click', '#role-table_wrapper .btn-edit', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#role-table").dataTable().fnGetPosition(tr);
        var data = $("#role-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $scope.editform = true;
        $("#rolename").focus();
        $("#rolename").val(data.name);
        $scope.$apply();
        
    });
    $('body').on('click', '#role-table_wrapper .btn-delete', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#role-table").dataTable().fnGetPosition(tr);
        var data = $("#role-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.id;
        $("#title_txt").text('Are you sure to delete role ' + data.name + ' ?');
        $("#modal-delete").modal('show');
    });

    $scope.modalAdd = function () {
        IdEdit = -1;
        $scope.editform = true;
        clearForm();
    };

    $scope.save = function () {
        var _url = IdEdit == -1 ? 'role-insert' : 'role-update';
        $rootScope.block();
        
        $.ajax({
            type: "POST",
            url: _url,
            data: {
                id: IdEdit,
                name: $("#rolename").val()
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                if (parseInt(response.sts) == 1) {
                    clearForm();
                    table.ajax.reload();
                    $scope.editform = false;
                }
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            },
        });
    };
    $scope.deleterole = function () {
        $('#modal-delete').block({
            message: '<img src="/assets/images/_loadblock.gif"/>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
        });

        $.ajax({
            type: "POST",
            url: 'role-delete',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#modal-delete").unblock();
                table.ajax.reload();
                $("#modal-delete").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $("#modal-delete").unblock();
                $("#modal-delete").modal('hide');
            },
        });
    };

    function clearForm() {
        $("#rolename").val("");
    };
});