mainApp.controller('supplierCtrl', function ($scope, $rootScope) {
    var IdEdit = -1;
    $scope.editform = false;

    getSelect2Data("coa-get", "select-coa");
    getSelect2Data("partnergroup-get", "select-partner");
    getSelect2Data("country-get", "select-country");
    $("#select-country").change(function () {
        getSelect2Data("province-get?country=" + $(this).val(), "select-province", $("#select-province").val());
    });
    $("#select-province").change(function () {
        getSelect2Data("city-get?province=" + $(this).val(), "select-city", $("#select-city").val());
    });

    var table = $("#supplier-table").DataTable({
        autoWidth: true,
        stateSave: true,
        pagingType: "simple",
        language: { paginate: { 'next': 'Next &rarr;', 'previous': '&larr; Prev' } },
        processing: true,
        serverSide: true,
        ajax: "m.supplier-table",
        lengthMenu: [
            [5, 10, 20, 50],
            [5, 10, 20, 50]
        ],
        "columns": [
            { "data": "c_id", "className": "not-show" },
            { "data": "c_code" },
            { "data": "c_name" },
            { "data": "c_address" },
            { "data": "c_phone" },
            { "data": "c_contactperson" },
            { "data": "c_contactemail" },
            { "data": "c_status" },
            { "data": "action", "className": "dt-center" }
        ]
    });
    $('.dataTables_filter input[type=search]').attr('placeholder', 'Type to filter...');
    $('.dataTables_length select').select2({
        minimumResultsForSearch: "-1"
    });

    $scope.addsupplier = function () {
        IdEdit = -1;
        clearForm();
        $scope.editform = true;
    };

    $('body').on('click', '#supplier-table_wrapper .btn-edit', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#supplier-table").dataTable().fnGetPosition(tr);
        var data = $("#supplier-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.c_id;
        $scope.editform = true;

        $rootScope.block();
        $.ajax({
            type: 'GET',
            url: 'supplier-get?id=' + IdEdit,
            success: function (response) {
                var data = response.data;
                $("#s_code").val(data.c_code);
                $("#s_name").val(data.c_name);
                $("#s_phone").val(data.c_phone);
                $("#s_address").val(data.c_address);
                $("#s_contactperson").val(data.c_contactperson);
                $("#s_contactpersonphone").val(data.c_contactphone);
                $("#s_contactpersonemail").val(data.c_contactemail);
                $("#s_note").val(data.c_note);
                $("#select-coa").val(data.c_coa_id).trigger("change");
                $("#select-partner").val(data.c_partnergrp_id).trigger("change");
                $("#select-country").val(data.c_country_id).trigger("change");
                $("#select-province").val(data.c_province_id).trigger("change");
                $("#select-city").val(data.c_city_id).trigger("change");
                $scope.$apply();
                $rootScope.unblock();
            },
            error: function () {
                $rootScope.unblock();
            }
        });

    });
    $('body').on('click', '#supplier-table_wrapper .btn-delete', function () {
        var tr = $(this).parent().parent()[0];
        var rowIndex = $("#supplier-table").dataTable().fnGetPosition(tr);
        var data = $("#supplier-table").dataTable().fnGetData(rowIndex);
        IdEdit = data.c_id;
        $("#title_tt").text('Are you sure to delete supplier ' + data.c_name + ' ?');
        $("#modal-delete").modal('show');
    });
    
    $scope.save = function () {
        var _url = IdEdit == -1 ? 'm.supplier-insert' : 'm.supplier-update';
        $rootScope.block();
        $.ajax({
            type: "POST",
            url: _url,
            data: {
                id: IdEdit,
                code: $("#s_code").val(),
                name: $("#s_name").val(),
                phone: $("#s_phone").val(),
                address: $("#s_address").val(),
                contactperson: $("#s_contactperson").val(),
                contactphone: $("#s_contactpersonphone").val(),
                contactemail: $("#s_contactpersonemail").val(),
                note: $("#s_note").val(),
                coa: $("#select-coa").val(),
                partnergroup: $("#select-partner").val(),
                country: $("#select-country").val(),
                province: $("#select-province").val(),
                city: $("#select-city").val()
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                if (parseInt(response.sts) == 1) {
                    clearForm();
                    table.ajax.reload();
                    $scope.editform = false;
                }
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            },
        });
    };
    $scope.delete = function () {
        $rootScope.blockcomponent('modal-delete');
        $.ajax({
            type: "POST",
            url: 'm.supplier-delete',
            data: { id: IdEdit },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $rootScope.unblockcomponent('modal-delete');
                if (parseInt(response.sts) == 1) {
                    table.ajax.reload();
                }
                $("#modal-delete").modal('hide');
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblockcomponent('modal-delete');
                $("#modal-delete").modal('hide');
            },
        });
    };
    function getSelect2Data(url, elm, initvalue = null) {
        $.ajax({
            type: 'GET',
            url: url,
            success: function (response) {
                var data = response.data;
                $('#' + elm + '').find('option').remove();
                $('#' + elm + '').append('<option></option>');
                for (i = 0; i < data.length; i++) {
                    $('#' + elm + '').append('<option value="' + data[i].c_id + '">' + data[i].c_name + '</option>');
                }
                $('#' + elm + '').select2({
                    allowClear: true,
                    placeholder: "select"
                });
                if (initvalue) {
                    $("#" + elm).val(initvalue).trigger("change");
                }
            },
            error: function () {

            }
        });
    };

    function clearForm() {
        $("#s_code").val("");
        $("#s_name").val("");
        $("#s_phone").val("");
        $("#s_address").val("");
        $("#s_contactperson").val("");
        $("#s_contactpersonphone").val("");
        $("#s_contactpersonemail").val("");
        $("#s_note").val("");
    };
});