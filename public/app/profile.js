mainApp.controller('profileCtrl', function ($scope, $rootScope) {

    $("#select-sex").select2({
        allowClear: true,
        placeholder: "select"
    });

    $('#dob').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false,
        format: 'YYYY-MM-DD'
    }).on('change', function (e, date) {
        var a = new Date(date);
        year = a.getFullYear();
        month = (a.getMonth() + 1) < 10 ? "0" + (a.getMonth() + 1) : (a.getMonth() + 1);
        date = a.getDate() < 10 ? "0" + a.getDate() : a.getDate();
        $("#dobsend").val(year + "-" + month + "-" + date);
    });;

    $scope.save = function () {
        $rootScope.block();
        $.ajax({
            data: new FormData($("#form-biodata")[0]),
            dataType: 'json',
            processData: false,
            contentType: false,
            type: 'POST',
            url: 'update-profile',
            success: function (rsp) {
                $rootScope.notification(rsp.msg);
                $rootScope.unblock();
                $scope.$apply();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootScope.unblock();
            }
        });
    };
});