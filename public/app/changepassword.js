mainApp.controller('changePasswordCtrl', function ($scope, $rootScope) {
    $scope.save = function () {
        $rootscope.block();
        $.ajax({
            type: "POST",
            url: 'change-password',
            data: {
                oldpassword: $("#oldpassword").val(),
                newpassword: $("#newpassword").val(),
                rtypepassword: $("#rnewpassword").val()
            },
            success: function (rsp) {
                response = JSON.parse(rsp);
                $rootScope.notification(response.msg);
                $("#oldpassword").val("");
                $("#newpassword").val("");
                $("#rnewpassword").val("");
                $rootscope.unblock();
            },
            error: function (err) {
                $rootScope.notification(err.responseText);
                $rootscope.unblock();
            },
        });
    }
});