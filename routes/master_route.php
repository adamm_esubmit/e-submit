<?php
    Route::group(['middleware' => 'auth'],function(){
        Route::group(['middleware' => 'privilege.admin'],function(){
            // SUPPLIER
            Route::get('/m.supplier-index',array('as' => 'm.supplier-index','uses' => 'Master\SupplierController@index'));
            Route::get('/m.supplier-table',array('as' => 'm.supplier-table','uses' => 'Master\SupplierController@table'));
            Route::post('/m.supplier-insert',array('as' => 'm.supplier-insert','uses' => 'Master\SupplierController@insert'));
            Route::post('/m.supplier-update',array('as' => 'm.supplier-update','uses' => 'Master\SupplierController@update'));
            Route::post('/m.supplier-delete',array('as' => 'm.supplier-delete','uses' => 'Master\SupplierController@delete'));
        });
        // WITHOUT ADMIN PREVILEGE
        Route::get('/supplier-get',array('as' => 'supplier-get','uses' => 'Master\SupplierController@getSupplier'));
        Route::get('/coa-get',array('as' => 'coa-get','uses' => 'Component\SelectController@getCoa'));
        Route::get('/partnergroup-get',array('as' => 'partnergroup-get','uses' => 'Component\SelectController@getPartnerGroup'));
        Route::get('/country-get',array('as' => 'country-get','uses' => 'Component\SelectController@getCountry'));
        Route::get('/province-get',array('as' => 'province-get','uses' => 'Component\SelectController@getProvince'));
        Route::get('/city-get',array('as' => 'city-get','uses' => 'Component\SelectController@getCity'));
    });
