<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) return redirect('home');   
    else return redirect('login');   
    
});
Route::get('/login', array('as' => 'login', 'uses' => 'Auth\LoginController@index'));
Route::post('/login', array('as' => 'login', 'uses' => 'Auth\LoginController@login'));
Route::get('/logout', array('as' => 'login', 'uses' => 'Auth\LoginController@logout'));

Route::group(['middleware' => 'auth'],function(){
    Route::get('/home',array('as' => 'home','uses' => 'Home\HomeController@index'));
    Route::get('/access-denied',array('as' => 'access-denied','uses' => 'Home\HomeController@denied'));
    Route::get('/change-password',array('as' => 'change-password','uses' => 'Home\HomeController@changepassword'));
    Route::post('/change-password',array('as' => 'change-password','uses' => 'Home\HomeController@submitpassword'));
    Route::post('/open-shift',array('as' => 'open-shift','uses' => 'Home\HomeController@openshift'));
    Route::post('/use-shift',array('as' => 'use-shift','uses' => 'Home\HomeController@useshift'));
    Route::get('/update-profile',array('as' => 'update-profile','uses' => 'Home\HomeController@updateprofile'));
    Route::post('/update-profile',array('as' => 'update-profile','uses' => 'Home\HomeController@changeprofile'));

    Route::group(['middleware' => 'privilege.admin'],function(){
        // USER
        Route::get('/user-index',array('as' => 'user-index','uses' => 'AccessSetting\UserController@index'));
        Route::get('/user-table',array('as' => 'user-table','uses' => 'AccessSetting\UserController@table'));
        Route::post('/user-insert',array('as' => 'user-insert','uses' => 'AccessSetting\UserController@insert'));
        Route::post('/user-update',array('as' => 'user-update','uses' => 'AccessSetting\UserController@update'));
        Route::post('/user-activate',array('as' => 'user-activate','uses' => 'AccessSetting\UserController@activate'));
        Route::post('/user-resetpassword',array('as' => 'user-resetpassword','uses' => 'AccessSetting\UserController@resetpassword'));
        Route::post('/user-unlock',array('as' => 'user-unlock','uses' => 'AccessSetting\UserController@unlock'));
        // ROLE
        Route::get('/role-index',array('as' => 'role-index','uses' => 'AccessSetting\RoleController@index'));
        Route::get('/role-table',array('as' => 'role-table','uses' => 'AccessSetting\RoleController@table'));
        Route::post('/role-insert',array('as' => 'role-insert','uses' => 'AccessSetting\RoleController@insert'));
        Route::post('/role-update',array('as' => 'role-update','uses' => 'AccessSetting\RoleController@update'));
        Route::post('/role-delete',array('as' => 'role-delete','uses' => 'AccessSetting\RoleController@delete'));
        // USER ROLE
        Route::get('/userrole-index',array('as' => 'userrole-index','uses' => 'AccessSetting\RoleUserController@index'));
        Route::get('/userrole-table',array('as' => 'userrole-table','uses' => 'AccessSetting\RoleUserController@table'));
        Route::post('/userrole-save',array('as' => 'userrole-save','uses' => 'AccessSetting\RoleUserController@save'));
        // ROLE PERMISSION
        Route::get('/rolepermission-index',array('as' => 'rolepermission-index','uses' => 'AccessSetting\RolePermissionController@index'));
        Route::get('/rolepermission-table',array('as' => 'rolepermission-table','uses' => 'AccessSetting\RolePermissionController@table'));
        Route::post('/rolepermission-save',array('as' => 'rolepermission-save','uses' => 'AccessSetting\RolePermissionController@save'));
        
    });
    // Without Previlage Admin
    Route::get('/userroles-get',array('as' => 'userroles-get','uses' => 'Component\LovController@getroles'));
    Route::get('/rolepermission-get',array('as' => 'rolepermission-get','uses' => 'Component\LovController@getpermission'));
    Route::get('/usersunitwork-get',array('as' => 'usersunitwork-get','uses' => 'Component\LovController@getUsersUnitwork'));
    Route::get('/holding-get',array('as' => 'holding-get','uses' => 'Component\SelectController@getHolding'));
    Route::get('/company-get',array('as' => 'company-get','uses' => 'Component\SelectController@getCompany'));
    Route::get('/unitwork-get',array('as' => 'unitwork-get','uses' => 'Component\SelectController@getUnitwork'));
    
    
});