<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class RolePermission extends Eloquent
{
    public $table = 'T_ROLE_PERMISSIONS';
    public $primaryKey = 'ID';
    public $incrementing = false;
}