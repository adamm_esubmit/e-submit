<?php

namespace App\Http\Middleware;

use Closure;
use App\UserRole;
use App\RolePermission;
use App\Permission;
use App\Shift;
use Illuminate\Support\Facades\DB;
use Auth;

class PrivilageAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $routeToAccess = $request->route()->uri;
        $routeName = Permission::where('name','=',$routeToAccess)->first()->display_name;
        $hasRole = $this->hasRole($routeToAccess);
        if(!$hasRole["verify"]){
            if($request->ajax()){
                header('HTTP/1.1 405 Access Denied');
                die("Access to : | ". $routeName ." | is Denied");
            }
            else
               return redirect('access-denied');
        }
        else{
            $isOpenShift = Shift::where('c_users_id','=',Auth::user()->id)
                            ->where(DB::raw('trunc(c_createdon)'),'=', date("Y-m-d"))
                            ->count() == 0 ? false : true;
            if(!$isOpenShift){
                if($request->ajax()){
                    header('HTTP/1.1 405 Access Denied');
                    die("You must open shift first");
                }
                else
                   return redirect('home');
            }
        }
        return $next($request);
    }

    private function hasRole($route){
        $user_id = Auth::user()->id;
        $rolesTb = UserRole::select('role_id')->where('user_id','=',$user_id)->get();
        $roles = [];
        $routeName = '';
        foreach($rolesTb as $r)
            $roles[] = $r->role_id;
        // Check Permission
        $allPermissions = RolePermission::select('permission_id')
                        ->distinct()
                        ->whereIn('role_id',$roles)
                        ->get();
        
        $verify = false;
        foreach($allPermissions as $all){
            $permName = Permission::find(intval($all->permission_id));
            if($permName->name == $route){
               $verify = true;
               break;
            }
        }
        return [
            'verify' => $verify,
            'route' => ""
        ];
    }
}
