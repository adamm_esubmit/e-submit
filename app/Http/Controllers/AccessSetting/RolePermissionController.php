<?php

namespace App\Http\Controllers\AccessSetting;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\Role;
use App\UserRole;
use App\Permission;
use App\RolePermission;
use DataTables;

class RolePermissionController extends Controller
{
    public function index(Request $request){
        $request->session()->flash('current_link','rolepermission-index');
        return view('settings.rolepermission');
    }
    
    public function table(Request $request){
        $model = Role::select(array(
            DB::raw('c_id as id'),
            DB::raw('c_name as name'),
            'created_at',
            'updated_at'
        ));
        return DataTables::eloquent($model)
                ->addColumn('action', function(Role $user){
                    return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit Permissions" class="btn btn-sm btn-icon btn-info btn-edit"><i class="fa fa-edit"></i></button>';
                })
                ->rawColumns(['action'])
                ->make();
    }



    public function save(Request $request){
        $roleId = $request->roleId;
        $listPermission = $request->rolepermissions;
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            RolePermission::where('role_id','=',$roleId)->delete();
            foreach($listPermission as $perm){
                $newPerm = new RolePermission();
                $newPerm->role_id = $roleId;
                $newPerm->permission_id = $perm;
                $newPerm->created_at = date("Y-m-d H:i:s");
                $newPerm->updated_at = date("Y-m-d H:i:s");
                $newPerm->save();
            }
            $this->writelog("GRANT-PERMISSION",'SUCCESS',json_encode($listPermission));
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Saved successfully";
        }
        catch(Exception $e){
            $this->writelog("GRANT-PERMISSION",'FAILED',json_encode($listPermission));
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }
}
