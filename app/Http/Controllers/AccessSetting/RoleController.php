<?php

namespace App\Http\Controllers\AccessSetting;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\Role;
use App\UserRole;
use App\Permission;
use App\RolePermission;
use DataTables;

class RoleController extends Controller
{
    public function index(Request $request){
        $request->session()->flash('current_link','role-index');
        return view('settings.role');
    }
    
    public function table(Request $request){
        $model = Role::select(array(
            DB::raw('c_id as id'),
            DB::raw('c_name as name'),
            'created_at',
            'updated_at'
        ));
        return DataTables::eloquent($model)
                ->addColumn('action', function(Role $user){
                    return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-sm btn-icon btn-info btn-edit"><i class="fa fa-edit"></i></button>&nbsp;
                            <button type="button" data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-sm btn-icon btn-danger btn-delete"><i class="fa fa-trash"></i></button>';
                })
                ->rawColumns(['action'])
                ->make();
    }

    public function insert(Request $request){
        $validator = Validator::make(
            // Input
            array(
                'Name' => trim($request->name)
            ),
            // Rules
            array(
                'Name' => 'required|alpha_dash'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'alpha_dash' => ':attribute can only contain alphabets, numbers, (-) or (_)'
            )
        );
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $role = new Role();
                $role->c_name = strtoupper(trim($request->name));
                $role->created_at = date("Y-m-d H:i:s");
                $role->updated_at = date("Y-m-d H:i:s");
                $role->created_by = Auth::user()->id;
                $role->updated_by = Auth::user()->id;
                $role->save();
                $this->writelog("ROLE-INSERT",'SUCCESS');
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("ROLE-INSERT",'FAILED');
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function update(Request $request){
        $validator = Validator::make(
            // Input
            array(
                'Name' => trim($request->name)
            ),
            // Rules
            array(
                'Name' => 'required|alpha_dash'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'alpha_dash' => ':attribute can only contain alphabets, numbers, (-) or (_)'
            )
        );
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                DB::update('update T_ROLES set c_name = ? , updated_at = ?, updated_by = ?
                            where c_id = ?', [
                    strtoupper(trim($request->name)),
                    date("Y-m-d H:i:s"),
                    Auth::user()->id,
                    $request->id
                ]);
                $this->writelog("ROLE-UPDATE",'SUCCESS');
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("ROLE-UPDATE",'FAILED');
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function delete(Request $request){
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            RolePermission::where('role_id','=',$request->id)->delete();
            UserRole::where('role_id','=',$request->id)->delete();
            Role::where('c_id','=',$request->id)->delete();
            $this->writelog("ROLE-DELETE",'SUCCESS');
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Deleted successfully";
        }
        catch(Exception $e){
            $this->writelog("ROLE-DELETE",'FAILED');
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }
}
