<?php

namespace App\Http\Controllers\AccessSetting;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\Role;
use App\User;
use App\UserRole;
use App\Unitwork;
use App\UsersUnitwork;
use DataTables;

class UserController extends Controller
{
    public function index(Request $request){
        $request->session()->flash('current_link','user-index');
        return view('settings.user');
    }
    /**
     * pOffset
     * pLimit 
     * pSearch
     */
    public function table(Request $request){
        // dddd DD MMMM YYYY
        $model = User::select(array(
                        'id',
                        'username',
                        DB::raw("firstname|| ' ' ||lastname as name"),
                        'email',
                        'gender',
                        DB::raw("TO_CHAR(dob,'DD/MM/YYYY') as dob"),
                        'last_login',
                        'status'
                    ));
        return DataTables::eloquent($model)
                ->addColumn('action', function(User $user){
                    if($user->status == 'A')
                        return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-icon btn-sm btn-info btn-edit"><i class="fa fa-edit"></i></button>&nbsp;
                                <button type="button" data-toggle="tooltip" data-placement="left" title="Disable User" class="btn btn-icon btn-sm btn-danger btn-disable"><i class="fa fa-trash"></i></button>&nbsp;
                                <button type="button" data-toggle="tooltip" data-placement="left" title="Reset User Password" class="btn btn-icon btn-sm btn-primary btn-repass"><i class="feather icon-refresh-cw"></i></button>';
                    else if($user->status == 'N')
                        return '<button type="button" data-toggle="tooltip" data-placement="left" title="Enable User" class="btn btn-icon btn-sm btn-success btn-enable"><i class="feather icon-share"></i></button>';
                    else
                        return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-icon btn-sm btn-info btn-edit"><i class="fa fa-edit"></i></button>&nbsp;
                                <button type="button" data-toggle="tooltip" data-placement="left" title="Unlock User" class="btn btn-icon btn-sm btn-warning btn-unlock"><i class="feather icon-unlock"></i></button>&nbsp;
                                <button type="button" data-toggle="tooltip" data-placement="left" title="Reset User Password" class="btn btn-icon btn-sm btn-primary btn-repass"><i class="feather icon-refresh-cw"></i></button>';
                })
                ->editColumn('status', function(User $user){
                    $raw = '';
                    if($user->status == 'L')
                        $raw = '<span class="badge badge-pill badge-primary">Locked</span>';
                    else if($user->status == 'A')
                        $raw = '<span class="badge badge-pill badge-success">Active</span>';
                    else
                        $raw = '<span class="badge badge-pill badge-danger">Not Active</span>';
                    return $raw;
                })
                ->rawColumns(['action','status'])
                ->make();
    }

    public function insert(Request $request){
        $validator = Validator::make(
            // Input
            array(
                'Username' => trim($request->username),
                'Email' => trim($request->email),
                'Firstname' => trim($request->firstname),
                'Lastname' => trim($request->lastname),
                'Sex' => trim($request->sex),
                'Date of Birth' => trim($request->dob)
            ),
            // Rules
            array(
                'Username' => 'required|alpha_dash|unique:users,username',
                'Email' => 'required|email|unique:users,email',
                'Firstname' => 'required|alpha',
                'Lastname' => 'required|alpha',
                'Sex' => 'required|alpha',
                'Date of Birth' => 'required|date',
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'alpha_dash' => ':attribute can only contain an alphabets, numbers, (-) or (_)',
                'alpha' => ':attribute can only contain an alphabets',
                'unique' => ':attribute already used, use another one',
                'email' => 'Incorrect :attribute format',
                'date' => 'Incorrect :attribute format',
            )
        );
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $user = new User();
                $user->username = strtoupper(trim($request->username));
                $user->password = Hash::make('3nterprise');
                $user->email = strtoupper(trim($request->email));
                $user->firstname = strtoupper(trim($request->firstname));
                $user->lastname = strtoupper(trim($request->lastname));
                $user->gender = strtoupper(trim($request->sex));
                $user->dob = date_create(trim($request->dob));
                $user->created_at = date("Y-m-d H:i:s");
                $user->updated_at = date("Y-m-d H:i:s");
                $user->status = 'A';
                $user->login_attempt = 0;
                $user->save();
                // SAVE UNIT WORK
                $units = $request->unitusers;
                foreach($units as $unit){
                    $userUnit = new UsersUnitwork;
                    $userUnit->C_USERS_ID = $user->id;
                    $userUnit->C_STATUS = 'A';
                    $userUnit->C_UNITWORK_ID = $unit;
                    $userUnit->C_COMPANY_ID = Unitwork::getCompanyId($unit);
                    $userUnit->C_HOLDING_ID = Unitwork::getHoldingId($unit);
                    $userUnit->C_CREATEDBY = Auth::user()->id;
                    $userUnit->C_CREATEDON = date("Y-m-d H:i:s");
                    $userUnit->save();
                }
                $this->writelog("USER-INSERT",'SUCCESS', json_encode($request));
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("USER-INSERT",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function update(Request $request){
        $validator = Validator::make(
            // Input
            array(
                'Username' => trim($request->username),
                'Email' => trim($request->email),
                'Firstname' => trim($request->firstname),
                'Lastname' => trim($request->lastname),
                'Sex' => trim($request->sex),
                'Date of Birth' => trim($request->dob)
            ),
            // Rules
            array(
                'Username' => 'required|alpha_dash',
                'Email' => 'required|email',
                'Firstname' => 'required|alpha',
                'Lastname' => 'required|alpha',
                'Sex' => 'required|alpha',
                'Date of Birth' => 'required|date'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'alpha_dash' => ':attribute can only contain an alphabets, numbers, (-) or (_)',
                'alpha' => ':attribute can only contain an alphabets',
                'unique' => ':attribute already used, use another one',
                'email' => 'Incorrect :attribute format',
                'date' => 'Incorrect :attribute format',
            )
        );
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $user = User::find(intval($request->id));
                $user->username = strtoupper(trim($request->username));
                $user->email = strtoupper(trim($request->email));
                $user->firstname = strtoupper(trim($request->firstname));
                $user->lastname = strtoupper(trim($request->lastname));
                $user->gender = strtoupper(trim($request->sex));
                $user->dob = date_create(trim($request->dob));
                $user->updated_at = date("Y-m-d H:i:s");
                $user->save();
                // SAVE UNIT WORK
                UsersUnitwork::where('C_USERS_ID','=',$user->id)->delete();
                $units = $request->unitusers;
                foreach($units as $unit){
                    $userUnit = new UsersUnitwork;
                    $userUnit->C_USERS_ID = $user->id;
                    $userUnit->C_STATUS = 'A';
                    $userUnit->C_UNITWORK_ID = $unit;
                    $userUnit->C_COMPANY_ID = Unitwork::getCompanyId($unit);
                    $userUnit->C_HOLDING_ID = Unitwork::getHoldingId($unit);
                    $userUnit->C_CREATEDBY = Auth::user()->id;
                    $userUnit->C_CREATEDON = date("Y-m-d H:i:s");
                    $userUnit->save();
                }
                $this->writelog("USER-UPDATE",'SUCCESS',json_encode($request));
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("USER-UPDATE",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function activate(Request $request){
        $id = intval($request->id);
        $user = User::find($id);
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            $user->status = intval($request->sts) == 0 ? 'N' : 'A' ;
            $user->updated_at = date("Y-m-d H:i:s");
            $user->save();

            $user->status = intval($request->sts) == 0 ? $this->writelog("USER-DEACTIVATE",'SUCCESS') : $this->writelog("USER-ACTIVATE",'SUCCESS');
            DB::commit();
            $msg->sts = 1;
            $msg->msg = intval($request->sts) == 0 ? "Disable User Successfully" : "Enable User Successfully" ;
        }
        catch(Exception $e){
            $user->status = intval($request->sts) == 0 ? $this->writelog("USER-DEACTIVATE",'FAILED') : $this->writelog("USER-ACTIVATE",'FAILED');
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }

    public function resetpassword(Request $request){
        $id = intval($request->id);
        $user = User::find($id);
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            $user->password = Hash::make('3nterprise');
            $user->updated_at = date("Y-m-d H:i:s");
            $user->save();
            $this->writelog("USER-RESETPASSWORD",'SUCCESS');
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Reset Password successfully";
        }
        catch(Exception $e){
            $this->writelog("USER-RESETPASSWORD",'FAILED');
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }

    public function unlock(Request $request){
        $id = intval($request->id);
        $user = User::find($id);
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            $user->status = 'A';
            $user->login_attempt = 0;
            $user->updated_at = date("Y-m-d H:i:s");
            $user->save();
            $this->writelog("USER-UNLOCK",'SUCCESS');
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Unlock User successfully";
        }
        catch(Exception $e){
            $this->writelog("USER-UNLOCK",'FAILED');
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }
}
