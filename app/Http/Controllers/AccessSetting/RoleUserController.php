<?php

namespace App\Http\Controllers\AccessSetting;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\Role;
use App\User;
use App\UserRole;
use App\Permission;
use App\RolePermission;
use DataTables;

class RoleUserController extends Controller
{
    public function index(Request $request){
        $request->session()->flash('current_link','userrole-index');
        return view('settings.userrole');
    }
    
    public function table(Request $request){
        $model = User::select(array(
            'id',
            'username',
            'email',
            'last_login',
            'status'
        ));
        return DataTables::eloquent($model)
                ->addColumn('action', function(User $user){
                    return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit Role" class="btn btn-icon btn-sm btn-info btn-edit"><i class="fa fa-edit"></i></button>';
                })
                ->editColumn('status', function(User $user){
                    $raw = '';
                    if($user->status == 'L')
                        $raw = '<span class="badge badge-pill badge-primary">Locked</span>';
                    else if($user->status == 'A')
                        $raw = '<span class="badge badge-pill badge-success">Active</span>';
                    else
                        $raw = '<span class="badge badge-pill badge-danger">Not Active</span>';
                    return $raw;
                })
                ->rawColumns(['action','status'])
                ->make();
    }


    public function save(Request $request){
        $userId = $request->id;
        $UserRole = $request->userroles;

        $msg = new \stdClass();

        DB::beginTransaction();
        try{
            UserRole::where('user_id','=',$userId)->delete();
            $strRole = '';
            foreach($UserRole as $role){
                $roleU = new UserRole();
                $roleU->user_id = $userId;
                $roleU->role_id = $role;
                $roleU->created_at = date("Y-m-d H:i:s");
                $roleU->updated_at = date("Y-m-d H:i:s");
                $roleU->created_by = Auth::user()->id;
                $roleU->updated_by = Auth::user()->id;
                $roleU->save();
                $roleName = UserRole::getRoleName($role);
                $strRole .= $roleName . ', ';
            }
            $this->writelog("GRANT ROLE",'GRANT ACCESS '.$strRole.'TO '.UserRole::getUserName($userId).' SUCCESS');
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Saved successfully";
        }
        catch(Exception $e){
            $this->writelog("GRANT ROLE",'GRANT ACCESS TO '.UserRole::getUserName($userId).' FAILED');
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        
        
        return json_encode($msg);
    }
}
