<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Holding;
use App\Company;
use App\Unitwork;
use App\Coa;
use App\PartnerGroup;
use App\Country;
use App\Province;
use App\City;

class SelectController extends Controller
{
    public function getHolding(){
        $holdings = Holding::where('c_status','=','A')
                    ->orderBy('c_name','ASC')
                    ->get();
        
        return response()->json([
            'data' => $holdings
        ]);
    }
    public function getCompany(Request $request){
        $pId = $request->query('pId');
        $companies = Company::where('c_status','=','A')
                     ->where('c_holding_id','=',$pId)
                     ->orderBy('c_name','ASC')
                     ->get();
        return response()->json([
            'data' => $companies
        ]);
    }
    public function getUnitwork(Request $request){
        $cId = $request->query('cId');
        $unitworks = Unitwork::where('c_status','=','A')
                     ->where('c_company_id','=',$cId)
                     ->orderBy('c_name','ASC')
                     ->get();
        return response()->json([
            'data' => $unitworks
        ]);
    }
    public function getCoa(){
        $coas = Coa::where('c_status','=','A')
                ->where('c_holding_id','=',Auth::user()->holding_id)
                ->where('c_company_id','=',Auth::user()->company_id)
                ->where('c_unitwork_id','=',Auth::user()->unitwork_id)
                ->get();
        return response()->json([
            'data' => $coas
        ]);
    }
    public function getPartnerGroup(){
        $prnt = PartnerGroup::where('c_status','=','A')->get();
        return response()->json([
            'data' => $prnt
        ]);
    }
    public function getCountry(){
        $country = Country::where('c_status','=','A')->get();
        return response()->json([
            'data' => $country
        ]);
    }
    public function getProvince(Request $request){
        $countryId = $request->query('country');
        $province = Province::where('c_status','=','A')
                    ->where('c_country_id','=',$countryId)
                    ->get();
        return response()->json([
            'data' => $province
        ]);
    }
    public function getCity(Request $request){
        $provinceId = $request->query('province');
        $city = City::where('c_status','=','A')
                    ->where('c_province_id','=',$provinceId)
                    ->get();
        return response()->json([
            'data' => $city
        ]);
    }
}
