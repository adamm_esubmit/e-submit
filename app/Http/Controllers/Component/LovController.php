<?php

namespace App\Http\Controllers\Component;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\Role;
use App\UserRole;
use App\Permission;
use App\RolePermission;
use App\Unitwork;
use App\UsersUnitwork;

class LovController extends Controller
{
    public function getpermission(Request $request)
    {
        $roleId = $request->query('roleid');
        
        $treeData = [];
        $allData =  Permission::leftJoin('T_ROLE_PERMISSIONS', function($join) use ($roleId) {
                        $join->on('T_ROLE_PERMISSIONS.permission_id', '=', 'T_PERMISSIONS.id')
                             ->on('T_ROLE_PERMISSIONS.role_id', '=', intval($roleId));
                    })                    
                    ->select(
                        'T_PERMISSIONS.id',
                        'T_PERMISSIONS.display_name',
                        'T_PERMISSIONS.parent_id',
                        'T_PERMISSIONS.is_menu',
                        DB::raw('CASE NVL(T_ROLE_PERMISSIONS.permission_id,0) WHEN 0 THEN 0 ELSE 1 END as flag')
                    )
                    ->orderBy('T_PERMISSIONS.urut','asc')
                    ->get();
        
        foreach($allData as $data){
            if(intval($data->parent_id) == 0){
                $root = new \stdClass();
                $root->id = $data->id;
                $root->display_name = $data->display_name;
                $root->parent_id = $data->parent_id;
                $root->is_menu = $data->is_menu;
                $root->selected = intval($data->flag) == 1 ? true : false;
                $root->childs = $this->ModelingTree($root,$allData);    
                $treeData[] = $root;
            }
        }
       
       return json_encode($treeData);
    }

    private function ModelingTree($root,$allData)
    {
        $childs = [];
        foreach($allData as $data){
            if(intval($data->parent_id) == intval($root->id)){
               $node = new \stdClass();
               $node->id = $data->id;
               $node->display_name = $data->display_name;
               $node->parent_id = $data->parent_id;
               $node->is_menu = $data->is_menu;
               $node->selected = intval($data->flag) == 1 ? true : false;
               $node->childs = $this->ModelingTree($node,$allData);
               $childs[] = $node;
            }
        }
        return $childs;
    }

    public function getroles(Request $request)
    {
        $userId = $request->query('userid');
        $allRoles = Role::all();
        if(intval($userId) != -1){
            $UserRole = UserRole::where('user_id','=',$userId)->orderBy('id','asc')->get();
            foreach($allRoles as $roles){
                $curr_role = intval($roles->c_id);
                $roles->selected = false;
                foreach($UserRole as $user){
                    if(intval($user->role_id) == $curr_role){
                        $roles->selected = true;
                        break;
                    }
                }
            }
        }
        else{
            foreach($allRoles as $roles){
                $roles->selected = false;
            }
        }
        return response()->json([
            'roles' => $allRoles,
        ]);
    }

    public function getUsersUnitwork(Request $request)
    {
        $userId = $request->query('userid');
        $allUnitwork = Unitwork::all();
        if(intval($userId) != -1){
            $list = UsersUnitwork::where('c_users_id','=',$userId)
                    ->where('c_status','=','A')
                    ->orderBy('C_UNITWORK_ID','ASC')
                    ->get();
            foreach($allUnitwork as $unit){
                $id = $unit->c_id;
                $unit->selected = false;
                foreach($list as $daftar){
                    if(intval($daftar->c_unitwork_id) == intval($id)){
                        $unit->selected = true;
                        break;
                    }
                }
            }
        }
        else{
            foreach($allUnitwork as $unit){
                $unit->selected = false;
            }
        }
        return response()->json([
            'data' => $allUnitwork
        ]);
    }
}
