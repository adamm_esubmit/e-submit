<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Supplier;
use DataTables;

class SupplierController extends Controller
{
    public function index(Request $request){
        $request->session()->flash('current_link','m.supplier-index');
        return view('master.supplier');
    }
    
    public function table(Request $request){
        $model = Supplier::select(array(
            'c_id',
            'c_code',
            'c_name',
            'c_address',
            'c_phone',
            'c_contactperson',
            'c_contactemail',
            'c_status'
        ));
        return DataTables::eloquent($model)
                ->addColumn('action', function(Supplier $user){
                    return '<button type="button" data-toggle="tooltip" data-placement="left" title="Edit" class="btn btn-sm btn-icon btn-info btn-edit"><i class="fa fa-edit"></i></button>&nbsp;
                            <button type="button" data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-sm btn-icon btn-danger btn-delete"><i class="fa fa-trash"></i></button>';
                })
                ->editColumn('c_status', function(Supplier $user){
                    $raw = '';
                    if($user->c_status == 'A')
                        $raw = '<span class="badge badge-pill badge-success">Active</span>';
                    else
                        $raw = '<span class="badge badge-pill badge-danger">Not Active</span>';
                    return $raw;
                })
                ->rawColumns(['action','c_status'])
                ->make();
    }

    public function insert(Request $request){
        $validator = Supplier::validator($request);
        
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $supplier = new Supplier();
                $supplier->c_code = trim($request->code);
                $supplier->c_name = trim($request->name);
                $supplier->c_address = trim($request->address);
                $supplier->c_phone = trim($request->phone);
                $supplier->c_contactperson = trim($request->contactperson);
                $supplier->c_contactphone = trim($request->contactphone);
                $supplier->c_contactemail = trim($request->contactemail);
                $supplier->c_coa_id = trim($request->coa);
                $supplier->c_partnergrp_id = trim($request->partnergroup);
                $supplier->c_note = trim($request->note);
                $supplier->c_status = 'A';
                $supplier->c_createdby = Auth::user()->id;
                $supplier->c_createdon = date("Y-m-d H:i:s");
                $supplier->c_holding_id = Auth::user()->holding_id;
                $supplier->c_company_id = Auth::user()->company_id;
                $supplier->c_unitwork_id = Auth::user()->unitwork_id;
                $supplier->c_country_id = trim($request->country);
                $supplier->c_province_id = trim($request->province);
                $supplier->c_city_id = trim($request->city);
                $supplier->save();
                $this->writelog("SUPPLIER-INSERT",'SUCCESS',json_encode($request));
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("SUPPLIER-INSERT",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function update(Request $request){
        $validator = Supplier::validator($request);
        
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $supplier = Supplier::where('c_id','=',$request->id)->first();
                $supplier->c_code = trim($request->code);
                $supplier->c_name = trim($request->name);
                $supplier->c_address = trim($request->address);
                $supplier->c_phone = trim($request->phone);
                $supplier->c_contactperson = trim($request->contactperson);
                $supplier->c_contactphone = trim($request->contactphone);
                $supplier->c_contactemail = trim($request->contactemail);
                $supplier->c_coa_id = trim($request->coa);
                $supplier->c_partnergrp_id = trim($request->partnergroup);
                $supplier->c_note = trim($request->note);
                $supplier->c_status = 'A';
                $supplier->c_modifiedby = Auth::user()->id;
                $supplier->c_modifiedon = date("Y-m-d H:i:s");
                $supplier->c_holding_id = Auth::user()->holding_id;
                $supplier->c_company_id = Auth::user()->company_id;
                $supplier->c_unitwork_id = Auth::user()->unitwork_id;
                $supplier->c_country_id = trim($request->country);
                $supplier->c_province_id = trim($request->province);
                $supplier->c_city_id = trim($request->city);
                $supplier->save();
                $this->writelog("SUPPLIER-UPDATE",'SUCCESS',json_encode($request));
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Saved successfully";
            }
            catch(Exception $e){
                $this->writelog("SUPPLIER-UPDATE",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function delete(Request $request){
        $msg = new \stdClass();
        DB::beginTransaction();
        try{
            $supplier = Supplier::where('c_id','=',$request->id)->first();
            $supplier->c_status = 'N';
            $supplier->c_deletedby = Auth::user()->id;
            $supplier->c_deletedon = date("Y-m-d H:i:s");
            $supplier->save();
            $this->writelog("SUPPLIER-DELETE",'SUCCESS',json_encode($request));
            DB::commit();
            $msg->sts = 1;
            $msg->msg = "Deleted successfully";
        }
        catch(Exception $e){
            $this->writelog("SUPPLIER-DELETE",'FAILED',json_encode($request));
            DB::rollBack();
            $msg->sts = 0;
            $msg->msg = $e->getMessage();
        }
        return json_encode($msg);
    }
    
    public function getSupplier(Request $request){
        $id = $request->query('id');
        $supplier = Supplier::find($id);
        return response()->json([
            'data' => $supplier
        ]);
    }
}
