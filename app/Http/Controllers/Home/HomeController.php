<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\UserRole;
use App\User;
use App\Permission;
use App\Logadmin;
use App\Shift;
use App\UsersUnitwork;
use App\Unitwork;
use App\Company;
use App\Holding;
use Auth;


class HomeController extends Controller
{ 
    public function index(){
        
        $listshift = Shift::where('c_users_id','=',Auth::user()->id)
                     ->orderBy('c_createdon','DESC')
                     ->skip(0)->take(10)
                     ->get();
        foreach($listshift as $shift){
            $shift->c_createdon = date_format(date_create($shift->c_createdon),"d-M-Y H:i");
            $shift->c_createdondate = $shift->c_createdon;
            $shift->holding = $shift::getHolding($shift->c_holding_id);
            $shift->company = $shift::getCompany($shift->c_company_id);
            $shift->unitwork = $shift::getUnitwork($shift->c_unitwork_id);
        }
        $usersunit = UsersUnitwork::where('c_status','=','A')
                    ->where('c_users_id','=',Auth::user()->id)
                    ->orderBy('c_createdon','ASC')
                    ->get();
        foreach($usersunit as $unit){
            $unit->unitname = Unitwork::find($unit->c_unitwork_id)->c_name;
            $unit->holdingname = Holding::find($unit->c_holding_id)->c_name;
            $unit->companyname = Company::find($unit->c_company_id)->c_name;
        }
        $image = \Storage::disk('sftp')->get(Auth::user()->photopath);
        \Storage::disk('local')->put("/public/photo.jpg", $image);

        return view('welcome')->with('data',[
            "shifts" => $listshift,
            "units" => $usersunit,
            "jsonunits" => json_encode($usersunit)
        ]);
    }

    public function denied(){
        return view('denied');
    }

    public function openshift(Request $request){

        $validator = Shift::validator($request);
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $isExistShiftByUser = Shift::where('c_users_id','=',Auth::user()->id)
                                      ->where('c_unitwork_id','=',trim($request->unitwork))
                                      ->where('c_company_id','=',trim($request->company))
                                      ->where('c_holding_id','=',trim($request->holding))
                                      ->where(DB::raw('trunc(c_createdon)'),'=', date("Y-m-d"))
                                      ->count() == 0 ? false : true;
                if(!$isExistShiftByUser){
                    $shift = new Shift();                
                    $shift->c_unitwork_id = trim($request->unitwork);
                    $shift->c_users_id = Auth::user()->id;
                    $shift->c_status = 'A';
                    $shift->c_createdby = Auth::user()->id;
                    $shift->c_createdon = date("Y-m-d H:i:s");
                    $shift->c_holding_id = trim($request->holding); 
                    $shift->c_company_id = trim($request->company); 
                    $shift->c_note = '';
                    $shift->save();
                    // SET USER
                    $userShift = User::find($shift->c_users_id);
                    $userShift->holding_id = trim($request->holding);
                    $userShift->company_id = trim($request->company);
                    $userShift->unitwork_id = trim($request->unitwork);
                    $userShift->save();
                    $this->writelog("OPEN-SHIFT",'SUCCESS',json_encode($request));
                    DB::commit();
                    $msg->sts = 1;
                    $msg->msg = "Open shift successfully";
                }
                else{
                    $msg->sts = 2;
                    $msg->msg = "Shift has been opened and is being used";
                }
            }
            catch(Exception $e){
                $this->writelog("OPEN-SHIFT",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public function useshift(Request $request){
        
        $validator = Shift::validator($request);
        $msg = new \stdClass();
        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $this->writelog("REUSE-SHIFT",'SUCCESS',json_encode($request));
                // SET USER
                $userShift = User::find(Auth::user()->id);
                $userShift->holding_id = trim($request->holding);
                $userShift->company_id = trim($request->company);
                $userShift->unitwork_id = trim($request->unitwork);
                $userShift->save();
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Open existing shift successfully";
            }
            catch(Exception $e){
                $this->writelog("REUSE-SHIFT",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }

    public static function getmenu($currlink){
        $user_id = Auth::user()->id;
        $rolesD = UserRole::select('role_id')->where('user_id','=',$user_id)->get();
        $roles = [];
        foreach($rolesD as $r)
            $roles[] = $r->role_id;
        // Permissions Parent
        $permissions = Permission::join('T_ROLE_PERMISSIONS','T_ROLE_PERMISSIONS.permission_id','=','T_PERMISSIONS.id')
                        ->select('T_PERMISSIONS.id','T_PERMISSIONS.name','T_PERMISSIONS.display_name','T_PERMISSIONS.icon','T_PERMISSIONS.parent_id','T_PERMISSIONS.is_menu')
                        ->where('T_PERMISSIONS.is_menu','=',1)
                        ->where('T_PERMISSIONS.parent_id','=',0)
                        ->whereIn('T_ROLE_PERMISSIONS.role_id',$roles)
                        ->orderBy('T_PERMISSIONS.urut','asc')
                        ->get();
        // Permission Child
        foreach($permissions as $key){
            $key->child = Permission::join('T_ROLE_PERMISSIONS','T_ROLE_PERMISSIONS.permission_id','=','T_PERMISSIONS.id')
            ->select('T_PERMISSIONS.id','T_PERMISSIONS.name','T_PERMISSIONS.display_name','T_PERMISSIONS.icon','T_PERMISSIONS.parent_id','T_PERMISSIONS.is_menu')
            ->where('T_PERMISSIONS.is_menu','=',1)
            ->where('T_PERMISSIONS.parent_id','=',$key->id)
            ->whereIn('T_ROLE_PERMISSIONS.role_id',$roles)
            ->orderBy('T_PERMISSIONS.urut','asc')
            ->get();
        }
        // Manipulate String
        $htmlmenu = '<ul class="nav pcoded-inner-navbar">';
        foreach($permissions as $perm){
            if(count($perm->child) > 0){
                $htmlmenu .= '<li &replace_active&>';
                $htmlmenu .= '<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa '.$perm->icon.'"></i></span><span class="pcoded-mtext">'.$perm->display_name.'</span></a>';
				$htmlmenu .= '<ul class="pcoded-submenu">';
                foreach($perm->child as $node){
                    if($currlink == $node->name){
                       $htmlmenu = str_replace('&replace_active&','class="nav-item pcoded-hasmenu pcoded-trigger active"',$htmlmenu);
                       $htmlmenu .= '<li class="active">';
                       $htmlmenu .= '<a href="/'.$node->name.'">'.$node->display_name.'</a>';
                       $htmlmenu .= '</li>';
                    }
                    else{
                        $htmlmenu = str_replace('&replace_active&','class="nav-item pcoded-hasmenu"',$htmlmenu);
                        $htmlmenu .= '<li>';
                        $htmlmenu .= '<a href="/'.$node->name.'">'.$node->display_name.'</a>';
                        $htmlmenu .= '</li>';
                    }
                }
                $htmlmenu .= '</ul>';
                $htmlmenu .= '</li>';
            }
            else{
                if($currlink == $perm->name){
                    $htmlmenu .= '<li class="active">';
                    $htmlmenu .= '<a href="/'.$perm->name.'">'.$perm->display_name.'</a>';
                    $htmlmenu .= '</li>';
                }
                else{
                    $htmlmenu .= '<li>';
                    $htmlmenu .= '<a href="/'.$perm->name.'">'.$perm->display_name.'</a>';
                    $htmlmenu .= '</li>';
                }
            }
        }
        $htmlmenu .= '</ul>';
        return html_entity_decode($htmlmenu);
    }

    public function changepassword(){
        return view('changepassword');
    }
    public function submitpassword(Request $request){
        $msg = new \stdClass();
        $validator = Validator::make(
            // Input
            array(
                'Old Password' => trim($request->oldpassword),
                'Password' => trim($request->newpassword),
                'Re-type Password' => trim($request->rtypepassword),
            ),
            // Rules
            array(
                'Old Password' => 'required',
                'Password' => 'required',
                'Re-type Password' => 'same:Password',
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'same' => 'password are not the same',
            )
        );
        if(!$validator->fails()){
            $user = User::where('id', '=', Auth::user()->id)->first();
            if (Hash::check($request->oldpassword, $user->password)) {
                $user->password = Hash::make($request->newpassword);
                $user->save();
                $msg->sts = 1;
                $msg->msg = "Successfully changed password";
                $this->writelog('CHANGE PASSWORD','SUCCESS');
            } else {
                $msg->sts = 0;
                $msg->msg = "Your old password are wrong";
                $this->writelog('CHANGE PASSWORD','FAILED');
            }
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
               $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        
        return json_encode($msg);
    }

    public function updateprofile(){
        $dob = Auth::user()->dob;
        $dob = date_format(date_create($dob),"Y-m-d");
        return view('updateprofile')->with('dob',$dob);
    }

    public function changeprofile(Request $request){

        $validator = User::validator($request);
        $msg = new \stdClass();

        if(!$validator->fails()){
            DB::beginTransaction();
            try{
                $user = User::find(Auth::user()->id);
                $user->username = strtoupper(trim($request->username));
                $user->firstname = strtoupper(trim($request->firstname));
                $user->lastname = strtoupper(trim($request->lastname));
                $user->email = strtoupper(trim($request->email));
                $user->gender = strtoupper(trim($request->sex));
                $user->dob = date_create(trim($request->dobsend));
                // SAVE PHOTO
                if($request->hasFile('photo')) {
                    $file = $request->photo;
                    $loc_path = md5(strtolower($user->username.$user->firstname));
                    \Storage::disk('sftp')->put('/images'."/".$loc_path.'/photo.'.$file->getClientOriginalExtension(), fopen($request->file('photo'), 'r+'));
                    $user->photopath = '/images'."/".$loc_path.'/photo.'.$file->getClientOriginalExtension();
                }
                $user->updated_at = date("Y-m-d H:i:s");
                $user->save();
                $this->writelog("UPDATED MY-PROFILE",'SUCCESS',json_encode($request));
                DB::commit();
                $msg->sts = 1;
                $msg->msg = "Update profile successfully";
            }
            catch(Exception $e){
                $this->writelog("UPDATED MY-PROFILE",'FAILED',json_encode($request));
                DB::rollBack();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }   
        }
        else{
            $messages = $validator->messages();
            $strMsg = '';
            foreach ($messages->all() as $message)
            $strMsg = $strMsg . '<i class="fa fa-check"></i>' . ' ' .$message . '<br>';
            $msg->sts = 0;
            $msg->msg = $strMsg;
        }
        return json_encode($msg);
    }
}
