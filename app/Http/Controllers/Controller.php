<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Logadmin;
use App\user;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function writelog($type,$desc,$jsondata = null)
    {
        $logadmin = new Logadmin();
        $logadmin->C_LOGTYPE = $type;
        $logadmin->C_DESC = $desc;
        $logadmin->C_STATUS = 'A';
        $logadmin->C_CODE = Auth::user()->username;
        $logadmin->C_CREATEDBY = Auth::user()->id;
        $logadmin->C_CREATEDON = date("Y-m-d H:i:s");
        $logadmin->C_DATA = $jsondata;
        $logadmin->save();
    }
    public static function writelogv2($type,$desc,$userid,$username,$jsondata = null)
    {
        $logadmin = new Logadmin();
        $logadmin->C_LOGTYPE = $type;
        $logadmin->C_DESC = $desc;
        $logadmin->C_STATUS = 'A';
        $logadmin->C_CODE = $username;
        $logadmin->C_CREATEDBY = $userid;
        $logadmin->C_CREATEDON = date("Y-m-d H:i:s");
        $logadmin->C_DATA = $jsondata;
        $logadmin->save();
    }
}
