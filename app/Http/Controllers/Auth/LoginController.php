<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Parameter;
use App\Logadmin;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    public function index(){
        if(Auth::check())
            return redirect('home');
        else
            return view('login');
    }

    public function login(Request $request){
        $validator = Validator::make(
            array(
                'Username' => trim($request->username),
                'Password' => $request->password
            ),
            array(
                'Username' => 'required | alpha_dash',
                'Password' => 'required'
            ),
            array(
                'required' => ':attribute is required',
                'alpha_dash' => ':attribute can only contain alphabets, numbers, (-) or (_)',
            )
        );
        
        $attempt = intval(Parameter::where('c_code','=','LOGINATTEMP')
                   ->where('c_status','=','A')
                   ->first()->c_value);
        if(!$validator->fails()){
            $userExist = User::where('username','=',strtoupper(trim($request->username)))->first();
            if($userExist){
                $userActive = User::where('username','=',strtoupper(trim($request->username)))
                            ->where('status','=','A')
                            ->first();
                if($userActive){
                    $userlogin = array(
                        'username' => strtoupper(trim($request->username)),
                        'password' => $request->password
                    );
                    if(Auth::attempt($userlogin,false)){
                        $userActive->last_login = date("Y-m-d H:i:s");
                        $userActive->login_attempt = 0;
                        $userActive->save();
                        $this->writelog('LOGIN','SUCCESS');
                        return redirect('home');
                    }
                    else{
                        $nAttempt = $userActive->login_attempt + 1;
                        $userActive->login_attempt = $nAttempt;
                        $userActive->save();
                        $this->writelogv2('LOGIN','LOGIN ATTEMPT '.$nAttempt,$userActive->id,$userActive->username);
                        if($nAttempt == $attempt){
                            $userActive->status = 'L';
                            $userActive->save();
                            return redirect('login')->with('msg','1');    
                        }
                        else
                            return redirect('login')->with('msg','2');    
                    }
                }
                else{
                    return redirect('login')->with('msg','3');
                }
            }
            else{
                return redirect('login')->with('msg','4');
            }
        }
        else{
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
    }

    public function logout(Request $request){
        $user = Auth::user();
        if($user){
            $this->writelog('LOGOUT','SUCCESS');
            Auth::logout();
        }
        return redirect('login');
    }
}
