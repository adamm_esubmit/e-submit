<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Company extends Eloquent
{
    public $table = 'T_COMPANY';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
