<?php

namespace App;
use App\Holding;
use App\Company;
use App\Unitwork;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Shift extends Eloquent
{
    public $table = 'T_SHIFT';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

    public static function getHolding($id){
        return Holding::find($id)->c_code;
    }

    public static function getCompany($id){
        return Company::find($id)->c_code;
    }

    public static function getUnitwork($id){
        return Unitwork::find($id)->c_name;
    }

    public static function validator(Request $request){
        return Validator::make(
            // Input
            array(
                'Unitwork' => trim($request->unitwork),
                'Company' => trim($request->company),
                'Holding' => trim($request->holding)
            ),
            // Rules
            array(
                'Unitwork' => 'required|numeric',
                'Company' => 'required|numeric',
                'Holding' => 'required|numeric'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'numeric' => ':attribute can only contain a numbers',
            )
        );
    }
}
