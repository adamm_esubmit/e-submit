<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Parameter extends Eloquent
{
    public $table = 'T_PARAMETER';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
