<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Province extends Eloquent
{
    public $table = 'T_PROVINCE';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
