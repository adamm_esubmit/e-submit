<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Permission extends Eloquent
{
    public $table = 'T_PERMISSIONS';
    public $primaryKey = 'ID';
    public $incrementing = false;
}