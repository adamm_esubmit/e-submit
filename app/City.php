<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class City extends Eloquent
{
    public $table = 'T_CITY';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
