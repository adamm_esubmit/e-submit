<?php

namespace App;

use App\Holding;
use App\Company;
use App\Unitwork;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class UsersUnitwork extends Eloquent
{
    public $table = 'T_USERS_UNITWORK';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
