<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;
use App\Role;
use App\User;

class UserRole extends Eloquent
{
    public $table = 'T_USER_ROLES';
    public $primaryKey = 'ID';
    public $incrementing = false;

    public static function getRoleName($id){
        return Role::find(intval($id))->name;
    }
    public static function getUserName($id){
        return User::find(intval($id))->username;
    }
}
