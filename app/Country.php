<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Country extends Eloquent
{
    public $table = 'T_COUNTRY';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
