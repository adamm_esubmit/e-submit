<?php

namespace App;

use App\Coa;
use App\PartnerGroup;
use App\Country;
use App\Province;
use App\City;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Supplier extends Eloquent
{
    public $table = 'T_SUPPLIER';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

    public static function validator(Request $request){
        Validator::extend('address_regex',function($attribute, $value, $parameters){
            $valid = false;
            if(preg_match('/^[a-zA-Z0-9.,-\/\\s]+$/', $value))
                $valid = true;
            else
                $valid = false;
            return $valid;
        });
        return Validator::make(
            // Input
            array(
                'Code' => trim($request->code),
                'Name' => trim($request->name),
                'Phone' => trim($request->phone),
                'Address' => trim($request->address),
                'Contact Person' => trim($request->contactperson),
                'Contact Phone' => trim($request->contactphone),
                'Contact Email' => trim($request->contactemail),
                'Note' => trim($request->note),
                'Coa' => trim($request->coa),
                'Partner Group' => trim($request->partnergroup),
                'Country' => trim($request->country),
                'Province' => trim($request->province),
                'City' => trim($request->city),
            ),
            // Rules
            array(
                'Code' => 'required|alpha_num',
                'Name' => 'required|regex:/^[a-zA-Z\s]+$/',
                'Phone' => 'required|numeric',
                'Address' => 'required|address_regex',
                'Contact Person' => 'required|regex:/^[a-zA-Z\s]+$/',
                'Contact Phone' => 'required|numeric',
                'Contact Email' => 'required|email',
                'Note' => 'regex:/^[a-zA-Z0-9\s]+$/',
                'Coa' => 'required|numeric',
                'Partner Group' => 'required|numeric',
                'Country' => 'required|numeric',
                'Province' => 'required|numeric',
                'City' => 'required|numeric'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'alpha_num' => ':attribute can only contain an alphabets or numbers',
                'numeric' => ':attribute can only contain a numbers',
                'address_regex' => ':attribute can only contain an alphabet, numbers, atau characters (,) (.) (-) (/)',
                'email' => ':attribute is not valid email'
            )
        );
    }

    protected function getCoa(){
        $coa = Coa::find($this->c_coa_id);
        return $coa->c_name . "(".$coa->c_account.")";
    }

    protected function getPartnerGroup(){
        $partner = PartnerGroup::find($this->c_partnergrp_id);
        return $partner->c_name;
    }

    protected function getCountry(){
        $country = Country::find($this->c_country_id);
        return $country->c_name;
    }

    protected function getProvince(){
        $province = Province::find($this->c_province_id);
        return $province->c_name;
    }

    protected function getCity(){
        $city = City::find($this->c_city_id);
        return $city->c_name;
    }
}
