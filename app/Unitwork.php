<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Unitwork extends Eloquent
{
    public $table = 'T_UNITWORK';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

    protected static function getCompanyId($unitworkid){
        $unitwork = Unitwork::find(intval($unitworkid));
        $company = Company::find(intval($unitwork->c_company_id));
        return $company->c_id;
    }

    protected static function getHoldingId($unitworkid){
        $unitwork = Unitwork::find(intval($unitworkid));
        $company = Company::find(intval($unitwork->c_company_id));
        $holding = Holding::find(intval($company->c_holding_id));
        return $holding->c_id;
    }
}
