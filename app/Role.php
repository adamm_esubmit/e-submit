<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Role extends Eloquent
{
    public $table = 'T_ROLES';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

}
