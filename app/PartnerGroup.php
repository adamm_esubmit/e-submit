<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class PartnerGroup extends Eloquent
{
    public $table = 'T_PARTNERGRP';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
