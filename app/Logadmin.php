<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Logadmin extends Eloquent
{
    public $table = 'T_LOGADMIN';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
