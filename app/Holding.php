<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Holding extends Eloquent
{
    public $table = 'T_HOLDING';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
