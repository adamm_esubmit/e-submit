<?php

namespace App;

use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Coa extends Eloquent
{
    public $table = 'T_COA';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
