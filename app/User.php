<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function validator(Request $request){
        $_input = [
            'Username' => trim($request->username),
            'Firstname' => trim($request->firstname),
            'Lastname' => trim($request->lastname),
            'Email' => trim($request->email),
            'Gender' => trim($request->sex),
            'Birth Date' => trim($request->dobsend),
            'Photo' => trim($request->photo)
        ];
        $_rules = [
            'Username' => 'required|alpha_num',
            'Firstname' => 'required|regex:/^[a-zA-Z\s]+$/',
            'Lastname' => 'required|regex:/^[a-zA-Z\s]+$/',
            'Email' => 'required|email',
            'Gender' => 'required',
            'Birth Date' => 'required'
        ];
        $_message = [
            'required' => ':attribute is required',
            'alpha_num' => ':attribute can only contain an alphabets or numbers',
            'numeric' => ':attribute can only contain a numbers',
            'regex' => ':attribute can only contain an alphabets or space',
            'email' => ':attribute is not valid email',
            'mimes' => ':attribute extension is not allowed',
            'max' => 'Size of :attribute too large'
        ];

        if($request->hasFile('photo')) {
            $rules_["Photo"] = "";     
        }
        else{
             $rules_["Photo"] = "";
        }

        return Validator::make($_input,$_rules,$_message);
    }
}
