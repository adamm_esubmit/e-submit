@extends('layout')

@section('extcss')
    
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Change Password</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-unlock-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Change Password</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="changePasswordCtrl">
    <div class="card-header">
        Protect your account by routinely changing passwords
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Old Password</label>
                        <div class="col-sm-6">
                            <input type="password" id="oldpassword" name="oldpassword" 
                                   class="form-control" placeholder="Old Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">New Password</label>
                        <div class="col-sm-6">
                            <input type="password" id="newpassword" name="newpassword" 
                                   class="form-control" placeholder="New Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Retype New Password</label>
                        <div class="col-sm-6">
                            <input type="password" id="rnewpassword" name="rnewpassword" 
                                   class="form-control" placeholder="RetypeNew Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-8 text-right" style="padding-right:0px;">
                            <button type="button" ng-click="save()" class="btn btn-success"><i class="fa fa-check-square"></i> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="app/changepassword.js" type="text/javascript"></script>
@endsection