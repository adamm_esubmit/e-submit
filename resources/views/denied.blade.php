@extends('layout')

@section('extcss')
    
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Access Denied</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-hammer"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Access Denied - Unauthorized (405)</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="text-center">
    <h1 class="mb-4">Restricted Page <br><i class="fa fa-user-alt-slash"></i></h1>
    <h5 class="text-muted mb-4">You do not have access to this page<br>Please contact the administrator</h5>
    <form action="/home" method="GET">
        <button class="btn btn-primary mb-4"><i class="feather icon-home"></i>Back to Home</button>
    </form>
</div>
@endsection

@section('extjs')
    
@endsection