<!DOCTYPE html>
<html lang="en">

<head>

	<title>Flash Able - Most Trusted Admin Template</title>
	<!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 11]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	
	<!-- Favicon icon -->
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
	<!-- fontawesome icon -->
	<link rel="stylesheet" href="assets/fonts/fontawesome/css/fontawesome-all.min.css">
	<!-- animation css -->
	<link rel="stylesheet" href="assets/plugins/animation/css/animate.min.css">
	<!-- vendor css -->
	<link rel="stylesheet" href="assets/css/style.css">
</head>

<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content container">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-6">
					<div class="card-body">
						<img src="assets/images/_logo.png" alt="" class="img-fluid mb-4">
                        <h4 class="mb-3 f-w-400">Login into your account</h4>
                        <form action="/login" method="POST" id="login-form">
                            @csrf
                            @if(Session::has('msg'))
                                @if(Session::get('msg') == '1')
                                <div class="alert alert-danger" role="alert">
                                    <strong><i class="fa fa-times-circle"></i>&nbsp;Oops, Sorry !</strong><br>
                                    Your password is incorrect for three times. Your user are locked now !
                                </div>
                                @elseif(Session::get('msg') == '2')
                                <div class="alert alert-warning" role="alert">
                                    <strong><i class="fa fa-times-circle"></i>&nbsp;Oops, Sorry !</strong><br>
                                    Your password is incorrect<br>If the error is up to 3 times, then your user will be locked !
                                </div>
                                @elseif(Session::get('msg') == '3')
                                <div class="alert alert-warning" role="alert">
                                    <strong><i class="fa fa-times-circle"></i>&nbsp;Oops, Sorry !</strong><br>
                                    Username has been locked, please contact admnistrator !
                                </div>
                                @elseif(Session::get('msg') == '4')
                                <div class="alert alert-info" role="alert">
                                    <strong><i class="fa fa-times-circle"></i>&nbsp;Oops, Sorry !</strong><br>
                                    Username not registered
                                </div>
                                @endif
                            @endif
                            @if($errors->has('Username') || $errors->has('Password'))
                            <div class="alert alert-danger" role="alert">
                                <strong><i class="fa fa-times-circle"></i>&nbsp;{{ $errors->first('Username') }}</strong><br>
                                <strong><i class="fa fa-times-circle"></i>&nbsp;{{ $errors->first('Password') }}</strong>
                            </div>
                            @endif
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                </div>
                                <input type="text" name="username" autocomplete="off" class="form-control" placeholder="Username">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                </div>
                                <input type="password" name="password" autocomplete="off" class="form-control" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-primary mb-4"><i class="fa fa-user-lock"></i>LOGIN</button>
                            <p class="mb-2 text-muted">Forgot password? <a href="auth-reset-password.html" class="f-w-400">Reset</a></p>
                        </form>
					</div>
				</div>
				<div class="col-md-6 d-none d-md-block">
					<!-- <img src="assets/images/_logo.jpeg" alt="" class="img-fluid"> -->
                    <img src="assets/images/auth-bg.jpg" alt="" class="img-fluid">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->

<!-- Required Js -->
<script src="assets/js/vendor-all.min.js"></script>
<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>


<div class="footer-fab">
    <div class="b-bg">
        <i class="fas fa-question"></i>
    </div>
    <div class="fab-hover">
        <ul class="list-unstyled">
            <li><a href="../doc/index-bc-package.html" target="_blank" data-text="UI Kit" class="btn btn-icon btn-rounded btn-info m-0"><i class="feather icon-layers"></i></a></li>
            <li><a href="../doc/index.html" target="_blank" data-text="Document" class="btn btn-icon btn-rounded btn-primary m-0"><i class="feather icon feather icon-book"></i></a></li>
        </ul>
    </div>
</div>


</body>

</html>
