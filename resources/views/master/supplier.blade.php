@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/data-tables/css/datatables.min.css">
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
    <style type="text/css">
        .not-show{display: none;}
        .dt-right{text-align: right;}
        .dt-left{text-align: left;}
        .dt-center{text-align: center;}
    </style>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Supplier</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-id-badge"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Master</a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Supplier</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="supplierCtrl">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-secondary"
                ng-click="addsupplier()">
            <i class="fa fa-id-badge"></i>Add Supplier
        </button>
    </div>
    <div class="card-body">
        <div ng-show="!editform" class="table-responsive">
            <table id="supplier-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="display:none;">Id</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Cont.Person</th>
                        <th>Cont.Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div ng-show="editform" class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Code</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Code"
                                autocomplete="off" id="s_code" name="s_code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Name"
                                autocomplete="off" id="s_name" name="s_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Phone"
                                autocomplete="off" id="s_phone" name="s_phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="4" autocomplete="off" id="s_address" name="s_address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cont.Person</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Contact Person"
                                autocomplete="off" id="s_contactperson" name="s_contactperson">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cont.Person (Phone)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Contact Person Phone"
                                autocomplete="off" id="s_contactpersonphone" name="s_contactpersonphone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Cont.Person (Email)</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Contact Person Email"
                                autocomplete="off" id="s_contactpersonemail" name="s_contactpersonemail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Note</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="4" autocomplete="off" id="s_note" name="s_note"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">COA</label>
                        <div class="col-sm-8">
                            <select id="select-coa" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Partner Group</label>
                        <div class="col-sm-8">
                            <select id="select-partner" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Country</label>
                        <div class="col-sm-8">
                            <select id="select-country" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Province</label>
                        <div class="col-sm-8">
                            <select id="select-province" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">City</label>
                        <div class="col-sm-8">
                            <select id="select-city" class="form-control"></select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-10 text-right" style="padding-right:5px;">
                            <button type="button" ng-click="editform=false;" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Close</button>
                            <button type="button" ng-click="save()" class="btn btn-primary btn-sm"><i class="fa fa-sd-card"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg"
         aria-hidden="true" role="dialog" id="modal-delete">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-body">
                    <h5 id="title_tt"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="delete()"><i class="fa fa-sd-card"></i>&nbsp;OK</button>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="app/master/supplier.js" type="text/javascript"></script>
@endsection