@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/data-tables/css/datatables.min.css">
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="assets/plugins/multi-select/css/multi-select.css">
    <style type="text/css">
        .not-show{display: none;}
        .dt-right{text-align: right;}
        .dt-left{text-align: left;}
        .dt-center{text-align: center;}
    </style>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>User Access Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-user-cog"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Settings</a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">User Access Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="userRoleCtrl">
    <div class="card-body">
        <div class="table-responsive">
            <table id="user-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Last Login</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" 
         aria-hidden="true" role="dialog" id="modal-edit">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit User Access</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <select id='role-users-ed' multiple='multiple'>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="save()"><i class="fa fa-sd-card"></i>&nbsp;Save</button>
                </div>   
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="app/setting/userRole.js" type="text/javascript"></script>
@endsection