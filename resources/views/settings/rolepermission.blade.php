@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/data-tables/css/datatables.min.css">
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="assets/plugins/multi-select/css/multi-select.css">
    <link rel="stylesheet" href="css/angular-ui-tree.min.css" type="text/css">
    <style type="text/css">
        .not-show{display: none;}
        .dt-right{text-align: right;}
        .dt-left{text-align: left;}
        .dt-center{text-align: center;}
        .angular-ui-tree-handle {
            background: #ffffff;
            border: 1px solid #e0cbf7;
            color: #7a17e6;
            padding: 5px 5px;
        }
        .angular-ui-tree-handle:hover {
            color: #7a17e6;
            background: #f4f6f7;
            border-color: #fcc726;
        }
        .angular-ui-tree-placeholder {
            background: #f0f9ff;
            border: 2px dashed #bed2db;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        tr.angular-ui-tree-empty {
            height:100px
        }
        .group-title {
            background-color: #687074 !important;
            color: #FFF !important;
        }
        .tree-node {
            border: 1px solid #dae2ea;
            background: #f8faff;
            color: #7c9eb2;
        }   
        .nodrop {
            background-color: #f2dede;
        }
        .tree-node-content {
            margin: 5px;
        }
        .tree-handle {
            padding: 5px;
            background: #428bca;
            color: #FFF;
            margin-right: 5px;
        }
        .angular-ui-tree-handle:hover {
        }
        .angular-ui-tree-placeholder {
            background: #f0f9ff;
            border: 2px dashed #bed2db;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .hidden {
            display: none!important;
            visibility: collapse!important;
        }
</style>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Role Access Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-share-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Settings</a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Role Access Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="rolePermissionCtrl">
    <div class="card-body">
        <div ng-show="isEdit == 0" class="table-responsive">
            <table id="role-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div ng-show="isEdit == 1" class="row">
            <div class="col-md-12">
                <div ui-tree data-nodrop-enabled="true" data-drag-enabled="false">
                    <ol ui-tree-nodes="" ng-model="data" id="tree-root">
                        <li ng-repeat="node in data" ui-tree-node ng-include="'nodes_renderer.html'" collapsed="true"></li>
                    </ol>
                </div>
                <br>
                <div class="form-group text-right" style="padding-right:0px;">
                    <button type="button" ng-click="isEdit=0;" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Close</button>
                    <button type="button" ng-click="save()" class="btn btn-sm btn-primary"><i class="fa fa-sd-card"></i>&nbsp;Save</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/ng-template" id="nodes_renderer.html">
        <div ui-tree-handle>
            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-success btn-sm" 
                        ng-if="node.childs && node.childs.length > 0"
                        ng-click="toggle(this)"
                        data-nodrag data-collapsed="true">
                            <span ng-class="{
                                'fa fa-angle-double-right': collapsed,
                                'fa fa-angle-double-down': !collapsed
                            }">
                        </span>
                    </a>&nbsp;
                    ^( node.display_name )^
                </div>
                <div class="col-md-6 text-right">
                    <div class="checkbox checkbox-danger d-inline">
                        <input type="checkbox" id="checkperm^( node.id )^" 
                               ng-model="node.selected" ng-checked="node.selected"
                               name="checkperm^( node.id )^"/>
                        <label for="checkperm^( node.id )^" class="cr"></label>
                    </div>
                </div>
            </div>
        </div>
        <ol ui-tree-nodes="" ng-model="node.childs" ng-class="{hidden: collapsed}">
            <li ng-repeat="node in node.childs" ui-tree-node ng-include="'nodes_renderer.html'" collapsed="true">
            </li>
        </ol>
    </script>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="app/setting/rolePermission.js" type="text/javascript"></script>
@endsection