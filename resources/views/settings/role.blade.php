@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/data-tables/css/datatables.min.css">
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="assets/plugins/multi-select/css/multi-select.css">
    <style type="text/css">
        .not-show{display: none;}
        .dt-right{text-align: right;}
        .dt-left{text-align: left;}
        .dt-center{text-align: center;}
    </style>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Role Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-share-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Settings</a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Role Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="roleCtrl">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-secondary"
                ng-click="modalAdd()">
            <i class="fa fa-share-alt"></i>Add Role
        </button>
    </div>
    <div class="card-body">
        <div ng-show="!editform" class="table-responsive">
            <table id="role-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div ng-show="editform" class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Role Name"
                                autocomplete="off" id="rolename" name="rolename">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 text-right" style="padding-right:5px;">
                            <button type="button" ng-click="editform=false;" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Close</button>
                            <button type="button" ng-click="save()" class="btn btn-primary btn-sm"><i class="fa fa-sd-card"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" 
         aria-hidden="true" role="dialog" id="modal-delete">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-body">
                    <h5 id="title_txt"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="deleterole()"><i class="fa fa-sd-card"></i>&nbsp;OK</button>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="assets/plugins/multi-select/js/jquery.quicksearch.js"></script>
    <script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="app/setting/role.js" type="text/javascript"></script>
@endsection