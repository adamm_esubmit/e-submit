@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/data-tables/css/datatables.min.css">
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="assets/plugins/multi-select/css/multi-select.css">
    <link rel="stylesheet" href="assets/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">\
    <link rel="stylesheet" href="assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="assets/fonts/material/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/plugins/mini-color/css/jquery.minicolors.css">

    <style type="text/css">
        .not-show{display: none;}
        .dt-right{text-align: right;}
        .dt-left{text-align: left;}
        .dt-center{text-align: center;}
    </style>
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>User Management</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-user-cog"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">Settings</a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">User Management</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="userCtrl">
    <div class="card-header">
        <button type="button" class="btn btn-sm btn-secondary"
                ng-click="modalAdd()">
            <i class="feather icon-user-plus"></i>Add User
        </button>
    </div>
    <div class="card-body">
        <div ng-show="!editform" class="table-responsive">
            <table id="user-table" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="display:none;">Id</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Sex</th>
                        <th>Birth</th>
                        <th>Last Login</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div ng-show="editform" class="row">
            <div class="col-md-12">
                <form>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Username"
                                autocomplete="off" id="username" name="username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Firstname</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Firstname"
                                autocomplete="off" id="firstname" name="firstname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Lastname</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Lastname"
                                autocomplete="off" id="lastname" name="lastname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Email"
                                autocomplete="off" id="email" name="email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Sex</label>
                        <div class="col-sm-8">
                            <select id="select-sex" class="form-control">
                                <option></option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Date of birth</label>
                        <input type="hidden" id="dobsend" value=""/>
                        <div class="col-sm-8">
                            <input type="text" id="dob" class="form-control" 
                                   placeholder="Date Of Birth">
                            
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">User's Unitwork</label>
                        <div class="col-sm-8">
                            <select multiple="multiple" id="select-usersunit">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 text-right" style="padding-right:5px;">
                            <button type="button" ng-click="editform=false;" class="btn btn-danger btn-sm"><i class="fa fa-times"></i>&nbsp;Close</button>
                            <button type="button" ng-click="save()" class="btn btn-primary btn-sm"><i class="fa fa-sd-card"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" 
         aria-hidden="true" role="dialog" id="modal-activate">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-body">
                    <h5 id="title_txt"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="activateuser()"><i class="fa fa-sd-card"></i>&nbsp;OK</button>
                </div>   
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" 
         aria-hidden="true" role="dialog" id="modal-repass">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-body">
                    <h5 id="title_tt"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="resetpassword()"><i class="fa fa-sd-card"></i>&nbsp;OK</button>
                </div>   
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" 
         aria-hidden="true" role="dialog" id="modal-unlock">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-body">
                    <h5 id="title_ttt"></h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" ng-click="unlockuser()"><i class="fa fa-sd-card"></i>&nbsp;OK</button>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script>
    <script src="assets/plugins/data-tables/js/datatables.min.js"></script>
    <script src="js/libs/moment-with-locales.min.js"></script>
    <script src="assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="assets/plugins/mini-color/js/jquery.minicolors.min.js"></script>
    <script src="app/setting/user.js" type="text/javascript"></script>
@endsection