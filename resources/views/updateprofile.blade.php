@extends('layout')

@section('extcss')
<link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="assets/plugins/material-datetimepicker/css/bootstrap-material-datetimepicker.css">\
<link rel="stylesheet" href="assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="assets/fonts/material/css/materialdesignicons.min.css">
<link rel="stylesheet" href="assets/plugins/mini-color/css/jquery.minicolors.css">
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>User's Profile</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="fa fa-unlock-alt"></i></a></li>
                    <li class="breadcrumb-item"><a href="javascript:;">User's Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card" ng-controller="profileCtrl">
    <div class="card-header">
        Update Your Profile
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form-biodata">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-6">
                            <input type="text" id="username" name="username" value="{{ Auth::user()->username }}"
                                   class="form-control" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Firstname</label>
                        <div class="col-sm-6">
                            <input type="text" id="firstname" name="firstname" value="{{ Auth::user()->firstname }}"
                                   class="form-control" placeholder="Firstname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Lastname</label>
                        <div class="col-sm-6">
                            <input type="text" id="lastname" name="lastname" value="{{ Auth::user()->lastname }}"
                                   class="form-control" placeholder="Lastname">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-6">
                            <input type="email" id="email" name="email" value="{{ Auth::user()->email }}"
                                   class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Sex</label>
                        <div class="col-sm-6">
                            <select id="select-sex" id="select-sex" name="sex" class="form-control">
                                <option></option>
                                @if(Auth::user()->gender == 'M')
                                    <option value="M" selected>Male</option>
                                    <option value="F">Female</option>
                                @else
                                    <option value="M">Male</option>
                                    <option value="F" selected>Female</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Date of birth</label>
                        <div class="col-sm-6">
                            <input type="hidden" id="dobsend" name="dobsend" value="{{ $dob }}"/>
                            <input type="text" id="dob" class="form-control" value="{{ $dob }}" 
                                   placeholder="Date Of Birth">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Photo</label>
                        <div class="col-sm-6">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="photo" name="photo">
                                <label class="custom-file-label" for="photo">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-8 text-left" style="padding-right:0px;">
                            <button type="button" ng-click="save()" class="btn btn-success"><i class="fa fa-check-square"></i> Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="js/libs/moment-with-locales.min.js"></script>
    <script src="assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="assets/plugins/mini-color/js/jquery.minicolors.min.js"></script>
    <script src="app/profile.js" type="text/javascript"></script>
@endsection