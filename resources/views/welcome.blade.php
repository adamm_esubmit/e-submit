@extends('layout')

@section('extcss')
    <link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">
@endsection

@section('breadcrumb')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-12">
                <div class="page-header-title">
                    <h5>Dashboard</h5>
                </div>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:;"><i class="feather icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row" ng-controller="homeCtrl">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <h5>Open Shift</h5>
            </div>
            <div class="card-body">
                <form>
                    <div class="form-group row">
                        <input type="hidden" id="jsondataunit" value="{{ $data['jsonunits'] }}">
                        <label class="col-sm-2 col-form-label">Unitwork</label>
                        <div class="col-sm-8">
                            <select id="select-unitworkos" class="form-control">
                                <option></option>
                                @foreach($data['units'] as $unit)
                                    <option value="{{ $unit->c_unitwork_id }}_{{ $unit->c_company_id }}_{{ $unit->c_holding_id }}">
                                        {{ $unit->unitname }}
                                    </option>    
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Company</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="comp_name" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Holding</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="hold_name" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10 text-left" style="padding-right:5px;">
                            <button type="button" ng-click="openshift()" class="btn btn-primary btn-sm">
                                <i class="fa fa-caret-square-right"></i>&nbsp;START SHIFT
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h5>Last user's open shift </h5>
            </div>
            <div class="card-body"> 
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><span>No.</span></th>
                                <th><span>Holding </span></th>
                                <th><span>Company </span></th>
                                <th><span>Unitwork </span></th>
                                <th><span>Open Date </span></th>
                                <th><span>Status </span></th>
                                <th><span>Action </span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $now = date_format(date_create(date("Y-m-d")), 'd-M-Y H:i'); 
                                  $nowdate = date("Y-m-d")
                            ?>
                            @if(count($data['shifts']) > 0)
                                <?php $i = 1;?>
                                @foreach($data['shifts'] as $shift)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $shift->holding }}</td>
                                    <td>{{ $shift->company }}</td>
                                    <td>{{ $shift->unitwork }}</td>
                                    <td>{{ $shift->c_createdon }}</td>
                                    <td>
                                        @if($shift->c_status = 'A')
                                            <span class="badge badge-success">OPEN</span>
                                        @else
                                            <span class="badge badge-warning">CLOSE</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($shift->c_holding_id == Auth::user()->holding_id &&
                                            $shift->c_company_id == Auth::user()->company_id &&
                                            $shift->c_unitwork_id == Auth::user()->unitwork_id &&
                                            $shift->c_createdon == $now)
                                            <span class="control-label">Already in use</span>
                                        @else
                                            @if($shift->c_createdondate > $nowdate)    
                                                <span class="control-label">CLOSE</span>
                                            @else
                                                <a href="javascript:;" ng-click="useshift(
                                                {{ $shift->c_holding_id }},
                                                {{ $shift->c_company_id }},
                                                {{ $shift->c_unitwork_id }} 
                                                )">USE</a>    
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                <?php $i++ ;?> 
                                @endforeach
                            @else
                                <tr>
                                    <td class="text-center" colspan="7">No records found</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h5>User's Profile</h5>
            </div>
            <div class="card-body text-center">
                <div class="usre-image">
                    <img src="{{ asset('storage/photo.jpg') }}" class="img-radius wid-100 m-auto" alt="User-Profile-Image">
                </div>
                <h6 class="f-w-600 m-t-25 m-b-10">{{ Auth::user()->firstname. ' ' .Auth::user()->lastname }}</h6>
                <p>Active | @if(Auth::user()->gender == 'M') Male @else Female @endif | Born {{ date_format(date_create(Auth::user()->dob),"d.M.Y") }}</p>
                <hr>
                <p class="m-t-15">Activity Level: 87%</p>
                <div class="bg-c-blue counter-block m-t-10 p-20">
                    <div class="row">
                        <div class="col-4">
                            <i class="fas fa-calendar-check text-white f-20"></i>
                            <h6 class="text-white mt-2 mb-0">1256</h6>
                        </div>
                        <div class="col-4">
                            <i class="fas fa-user text-white f-20"></i>
                            <h6 class="text-white mt-2 mb-0">8562</h6>
                        </div>
                        <div class="col-4">
                            <i class="fas fa-folder-open text-white f-20"></i>
                            <h6 class="text-white mt-2 mb-0">189</h6>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row justify-content-center user-social-link">
                    <div class="col-auto"><a href="#!"><i class="fab fa-facebook-f text-primary f-22"></i></a></div>
                    <div class="col-auto"><a href="#!"><i class="fab fa-twitter text-c-info f-22"></i></a></div>
                    <div class="col-auto"><a href="#!"><i class="fab fa-dribbble text-c-red f-22"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('extjs')
    <script src="js/libs/jquery-block.js"></script>
    <script src="assets/plugins/select2/js/select2.full.min.js"></script>
    <script src="app/main.js" type="text/javascript"></script>
@endsection